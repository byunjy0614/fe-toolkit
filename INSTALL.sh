#!/bin/bash
set -e

#########################################################################
# Set the compilers

export CC=gcc
export CXX=g++

export MPICXX=$(type -P mpic++)
export MPICC=$(type -P mpicc)

if [ "${MPICXX}" == "" -o "${MPICC}" == "" ]; then
    echo "You need to install MPI"
    echo "On fedora, you can type:"
    echo "   sudo dnf install mpich.x86_64 mpich-devel.x86_64"
    echo "and then activate it with:"
    echo "   module load mpi/mpich-x86_64"
    exit 1
fi




#########################################################################
# Set installation target directory

export PREFIX=${PWD}/local
export LIBDIR=${PREFIX}/lib
export LIB64DIR=${PREFIX}/lib64
export BINDIR=${PREFIX}/bin
export INCDIR=${PREFIX}/include




#########################################################################
# Set the compiler flags

export CXXFLAGS="-O3 -std=c++0x -march=native -mtune=native -Wall -Wextra"
export CPPFLAGS="-DNDEBUG"
# I placed -lstdc++ because I've seen some reports from users
# who used a MPICXX that did not link to the c++ library, for
# whatever reason... seems like a system configuration problem on
# thier end, but no harm to include it here
export LDFLAGS="${LDFLAGS} -L${LIBDIR} -L${LIB64DIR} -lstdc++"
export LIBRARY_PATH="${LIBDIR}:${LIB64DIR}:${LIBRARY_PATH}"
export LD_LIBRARY_PATH="${LIBDIR}:${LIB64DIR}:${LD_LIBRARY_PATH}"
export CPATH="${PREFIX}/include:${CPATH}"
export PYTHONPATH="${LIBDIR}/python${PYTHONVER}/site-packages:${PYTHONPATH}"




#########################################################################
# Make ${PREFIX}/lib64, if it is missing

if [ ! -d ${LIB64DIR} ]; then
   mkdir -p ${LIB64DIR}
fi


#########################################################################
# Set the python interpreter

export PYTHON=`which python3`

PYTHONVER=$(${PYTHON} -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')

MAJORVERSION=$(echo ${PYTHONVER} | sed "s/\..*$//")
if [ "${MAJORVERSION}" != "3" ]; then
    echo "python3 version is ${PYTHONVER}, but version 3 is required"
    exit 1
fi

MINORVERSION=$(echo $(bc -l <<< "(${PYTHONVER} - 3)") | sed -e "s/^.*\.//")
VERSIONCHECK=$(bc -l <<< "${MINORVERSION} >= 6")

if [ "${VERSIONCHECK}" != "1" ]; then
   echo "python3 version is ${PYTHONVER}, but a version >= 3.6 is required"
   exit 1
fi




#########################################################################
# Install dependencies


# == BLAS/LAPACK ==
# If Intel MKL is installed (either MKL_HOME or MKLHOME or MKLROOT
# point to the installation path), then the configure script will
# detect it. Otherwise, install openblas

echo "Looking for BLAS/LAPACK..."
if [ "${MKL_HOME}" == "" ]; then
    if [ "${MKLHOME}" != "" ]; then
	export MKL_HOME=${MKLHOME}
	echo "Assuming MKL is installed because MKLHOME is set"
    elif [ "${MKLROOT}" != "" ]; then
	export MKL_HOME=${MKLROOT}
	echo "Assuming MKL is installed because MKLROOT is set"
    else
	export LAPACK_LIBS="-lopenblas"
	
	if [[ `echo "int main(){}" | gcc -x c++ -Wl,--no-as-needed ${LDFLAGS} -lopenblas - && ldd a.out | grep openblas` ]] &>/dev/null; then
	    NEEDOPENBLAS=false
	else
	    NEEDOPENBLAS=true
	fi


	if [ -e a.out ]; then
	    rm -f a.out
	fi

	
	if [ "${NEEDOPENBLAS}" = true ]; then
	    echo "BLAS/LAPACK not found.  Installing openblas..."
	    git clone -b 'v0.3.8' --single-branch --depth 1 https://github.com/xianyi/OpenBLAS.git
	    cd OpenBLAS
	    make USE_THREAD=0 USE_LOCKING=1 && make PREFIX=${PREFIX} install
	    cd ../
	    if [ ! -e ${LIBDIR}/libopenblas.so -a ! -e ${LIB64DIR}/libopenblas.so ]; then
		echo "Failed to build libopenblas.so"
		exit 1
	    fi
	else
	    echo "Found pre-installed openblas implementation of BLAS/LAPACK library using -lopenblas" 
	fi
    fi
else
    echo "Assuming MKL is installed because MKL_HOME is set"
fi





# == NLOPT ==

echo "Looking for nlopt..."

export NLOPT_LIBS="-lnlopt"
if [[ `echo -e "#include <nlopt.hpp>\nint main(){}" | gcc -x c++ -Wl,--no-as-needed ${LDFLAGS} -lnlopt - && ldd a.out | grep nlopt` ]] &>/dev/null; then
    NEEDNLOPT=false
else
    NEEDNLOPT=true
fi

if [ -e a.out ]; then
    rm -f a.out
fi

if [ "${NEEDNLOPT}" = true ]; then
    echo "nlopt not found.  Installing nlopt..."

    CMAKE=$(type -P cmake)
    if [ "${CMAKE}" == "" ]; then
	#${PYTHON} -m venv ${PREFIX}
	${PYTHON} -m pip install cmake
    fi
    if [ -e ${BINDIR}/cmake ]; then
	CMAKE=${BINDIR}/cmake
    fi

    if [ ! -d nlopt ]; then
	git clone -b 'v2.6.1' --single-branch --depth 1 https://github.com/stevengj/nlopt
    fi
    cd nlopt
    if [ ! -d build ]; then
	mkdir build
    fi
    cd build
    ${CMAKE} -DCMAKE_INSTALL_PREFIX=${PREFIX} -DNLOPT_PYTHON=OFF ..
    make
    make install
    cd ../../
    if [ ! -e "${LIBDIR}/libnlopt.so" -a ! -e "${LIB64DIR}/libnlopt.so" ]; then
	echo "Failed to build libnlopt.so"
	exit 1
    fi
else
    echo "Found pre-installed nlopt library using -lnlopt" 
fi






#########################################################################
# Install graphmbar

echo "Compiling graphmbar"

cd src
tar -xzf graphmbar-0.9.tar.gz
cd graphmbar-0.9
./configure --with-mpi \
	    --prefix=${PREFIX} \
	    LDFLAGS="${LDFLAGS}" \
            CXXFLAGS="${CXXFLAGS}" \
            CPPFLAGS="${CPPFLAGS}" \
	    CC="${MPICC}" \
	    CXX="${MPICXX}"

make
make install
cd ../../



#########################################################################
# Install ndfes

echo "Compiling ndfes"
cd src
tar -xzf ndfes-1.6.tar.gz
cd ndfes-1.6
./configure --with-openmp \
	    --prefix=${PREFIX} \
            CXXFLAGS="${CXXFLAGS}" \
            CPPFLAGS="${CPPFLAGS}" \
	    LDFLAGS="${LDFLAGS}"
make
make install
cd ../../



#########################################################################
# Install python companion libraries

echo "Using pip to install graphmbar companion python library"
cd src/graphmbar-0.9
PYTHONUSERBASE=${PREFIX} python3 -m pip install --prefix=${PREFIX} src/python
cd ../../

echo "Using pip to install ndfes companion python library"
cd src/ndfes-1.6
PYTHONUSERBASE=${PREFIX} python3 -m pip install --prefix=${PREFIX} src/python
cd ../../

echo ""
echo ""
echo "If pip fails to install the python companion libraries because it"
echo "couldn't build 'pillow', then it likely means you need to install"
echo "the libjpeg-devel package. On fedora, you can install this with:"
echo "sudo dnf install libjpeg-devel"
echo "For more information, including its installation on other OSs, see: "
echo "   https://stackoverflow.com/questions/34631806/fail-during-installation-of-pillow-python-module-in-linux"

#########################################################################
# Create some environmental variable settings


cat <<EOF > fetoolkit.bashrc
#!/bin/bash

#
# This file modifies your environmental variables such that
# the installation directories are included in
# your PATH, LD_LIBRARY_PATH, and PYTHONPATH and activates
# the python virtual environment, which includes all
# necessary python dependencies.
#
# To load the environmental variables, you can source this
# file or edit your ~/.bashrc accordingly.
#
# Alternatively -- and recommended -- is to NOT use this
# file nor modify your bashrc. Instead, consider using
# environemental modules, which more easily allows you
# to both add and remove changes to your environment.
# A fetoolkit module file is also included, and shown
# below. 
#

echo "Modifying environment to use fetoolkit"

export PATH="${BINDIR}:\${PATH}"
export LD_LIBRARY_PATH="${LIBDIR}:${LIB64DIR}:\${LD_LIBRARY_PATH}"
export PYTHONPATH="${LIBDIR}/python${PYTHONVER}/site-packages:${PYTHONPATH}"

EOF


cat <<EOF > fetoolkit.module
#%Module 1.0

## To use this module without root permissions,
##    mkdir \${HOME}/modulefiles
## Then add the following line to your \${HOME}/.bashrc file:
##    export MODULEPATH="\${HOME}/modulefiles:\${MODULEPATH}"
## Source your edited bashrc file
##    source \${HOME}/.bashrc
## Copy this file to the module directory; that is,
##    cp fetoolkit.module \${HOME}/modulefiles/fetoolkit
## You can now load/unload the environment with
##    module load fetoolkit
##    module unload fetoolkit
##
##



prepend-path PATH            ${BINDIR}
prepend-path LD_LIBRARY_PATH ${LIBDIR}
prepend-path LD_LIBRARY_PATH ${LIB64DIR}
append-path PYTHONPATH      ${LIBDIR}/python${PYTHONVER}/site-packages

EOF



cat <<EOF | tee ACTIVATE.txt


#########################################################################
#########################################################################


Installation is complete.

To use the analysis tools,
1. prepend your PATH environmental variable with ${BINDIR}
2. prepend your LD_LIBRARY_PATH environmental variable with ${LIBDIR}
3. prepend your your LD_LIBRARY_PATH environmental variable with ${LIB64DIR}
4. set your PYTHONPATH to ${LIBDIR}/python${PYTHONVER}/site-packages

The above actions can be performed by sourcing the provided
fetoolkit.bashrc file

Please see fetoolkit.module for instructions on how to use 
environmental modules to more easily preapre your environment

EOF
