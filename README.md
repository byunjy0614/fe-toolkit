This repository is a collection of programs useful for free energy analysis.
The programs are located within the src/ directory. Each program has its own
license, which can be viewed by extracting the contents of the program
distribution tarball.

The programs included in this distribution include:


ndfes     - a program for evaluating free energy profiles from umbrella
            window simulations using either vFEP or MBAR analysis.

graphmbar - a program for performing networkwide BAR or MBAR analysis
            of alchemical free energy transformation graphs typically
	    constructed to compute relative binding (or solvation)
	    free energies.

Installation instructions and program-specific documentation can be found
in the doc/ directory.

We request that if you use this software in a publication, to please reference
as appropriate:

[1] Alchemical Binding Free Energy Calculations in AMBER20: Advances and Best 
Practices for Drug Discovery
Tai-Sung Lee, Bryce K. Allen, Timothy J. Giese, Zhenyu Guo, Pengfei Li, 
Charles Lin, T. Dwight McGee, David A. Pearlman, Brian K. Radak, Yujun Tao, 
Hsu-Chun Tsai, Huafeng Xu, Woody Sherman, Darrin M. York
J. Chem. Inf. Model. (2020) 60, 5595-5623
DOI: 10.1021/acs.jcim.0c00613

[2] Variational Method for Networkwide Analysis of Relative Ligand Binding Free 
Energies with Loop Closure and Experimental Constraints
Timothy J. Giese, Darrin M. York
J. Chem. Theory Comput. (2021)
DOI: 10.1021/acs.jctc.0c01219

[3] Extension of the Variational Free Energy Profile and Multistate Bennett 
Acceptance Ratio Methods for High-Dimensional Potential of Mean Force Profile 
Analysis
Timothy J. Giese, Şölen Ekesan, Darrin M. York
J. Phys. Chem. A (2021) 125, 4216-4232
DOI: 10.1021/acs.jpca.1c00736

